/*Yusi "Justin" Cheng
 * CS130-2014
 * Java Assignment 3
 */
import javax.swing.*;

public class Assignment3 {
	static String AdminUsername = "Justin";
	static String StaffUsername = "Seth";
	static String StudentUsername = "Leonardo";
	static String Iusername = "x";
	static int count = 0;
    
	public static void main(String args[]) {
		String[] choices = { "Admin", "Student", "Staff" };
		String input = (String) JOptionPane.showInputDialog(null,
                                                            "Choose account type...", "Account Type",
                                                            JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
		int i = 0;
		if (input == "Admin") {
			i = 1;
		} else if (input == "Student") {
			i = 2;
		} else if (input == "Staff") {
			i = 3;
		}
        
		switch (i) {
            case 1:
                Admin();
                break;
            case 2:
                Student();
                break;
            case 3:
                Staff();
                break;
		}
	}
    
	public static void AccountType() {
		String[] choices = { "Admin", "Student", "Staff" };
		String input = (String) JOptionPane.showInputDialog(null,
                                                            "Wrong Usertype.Rechoose account type", "Account Type",
                                                            JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
		int i = 0;
		if (input == "Admin") {
			i = 1;
		} else if (input == "Student") {
			i = 2;
		} else if (input == "Staff") {
			i = 3;
		}
        
		switch (i) {
            case 1:
                Admin();
                break;
            case 2:
                Student();
                break;
            case 3:
                Staff();
                break;
		}
	}
    
	/*
	 * public static String Iusername() { String Iusername =
	 * JOptionPane.showInputDialog("Enter Your Username"); return Iusername; }
	 * public static String Iusername = Iusername();
	 */
	public static void Admin() {
		boolean bresult1 = false;
		Iusername = JOptionPane.showInputDialog("Enter Your Username");
        
		if (Iusername.equals(StudentUsername)) {
			AccountType();
		} else if (Iusername.equals(StaffUsername)) {
			AccountType();
		} else if (Iusername.equals(AdminUsername)) {
			bresult1 = true;
		} else {
			bresult1 = false;
		}
		if (bresult1 == true) {
			adminpassword();
		} else if (bresult1 == false) {
			while (count < 2) {
				count++;
				Iusername = JOptionPane
                .showInputDialog("Username doesn't exist.Reenter Your Username");
				if (Iusername.equals(AdminUsername)) {
					adminpassword();
					bresult1 = true;
					break;
				} else if ((count == 2) && (bresult1 == true)) {
					adminpassword();
					break;
				} else {
					bresult1 = false;
				}
			}
		}
		if ((count >= 2) && (bresult1 == false)) {
			JOptionPane.showMessageDialog(null,
                                          "Invalid Username. Contact Admin", "Locked",
                                          JOptionPane.ERROR_MESSAGE);
		}
	}
    
	public static void adminpassword() {
		String Ipassword;
		String CorrectPassword = "Wade'931026";
		boolean bresult2 = false;
		Ipassword = JOptionPane.showInputDialog("Enter Your Password");
		if (Ipassword.equals(CorrectPassword)) {
			bresult2 = true;
			JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                          "Welcome", JOptionPane.PLAIN_MESSAGE);
		} else if ((count >= 2) && (bresult2 == false)) {
			JOptionPane.showMessageDialog(null,
                                          "Account Locked. Contact Admin", "Locked",
                                          JOptionPane.ERROR_MESSAGE);
		} else
			while (count < 2) {
				count++;
				Ipassword = JOptionPane
                .showInputDialog("Wrong password.Reenter Your Password");
				if (Ipassword.equals(CorrectPassword)) {
					JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                                  "Welcome", JOptionPane.PLAIN_MESSAGE);
					break;
				} else if ((count == 2) && (bresult2 == true)) {
					JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                                  "Welcome", JOptionPane.PLAIN_MESSAGE);
					break;/*
                           * solve the problem when count = 2 & bresult2 = 2, the
                           * application shows up with both welcome and account
                           * locked
                           */
				} else {
					bresult2 = false;
				}
				if ((count >= 2) && (bresult2 == false)) {
					JOptionPane.showMessageDialog(null,
                                                  "Account Locked. Contact Admin", "Locked",
                                                  JOptionPane.ERROR_MESSAGE);
				}
			}
	}
    
	public static void Student() {
		boolean bresult1 = false;
		Iusername = JOptionPane.showInputDialog("Enter Your Username");
		if (Iusername.equals(AdminUsername)) {
			AccountType();
		} else if (Iusername.equals(StaffUsername)) {
			AccountType();
		} else if (Iusername.equals(StudentUsername)) {
			bresult1 = true;
		} else {
			bresult1 = false;
		}
		if (bresult1 == true) {
			studentpassword();
		} else if (bresult1 == false) {
			while (count < 2) {
				count++;
				Iusername = JOptionPane
                .showInputDialog("Username doesn't exist.Reenter Your Username");
				if (Iusername.equals(StudentUsername)) {
					studentpassword();
					bresult1 = true;
					break;
				} else if ((count == 2) && (bresult1 == true)) {
					studentpassword();
					break;
				} else {
					bresult1 = false;
				}
			}
		}
		if ((count >= 2) && (bresult1 == false)) {
			JOptionPane.showMessageDialog(null,
                                          "Invalid Username. Contact Admin", "Locked",
                                          JOptionPane.ERROR_MESSAGE);
		}
	}
    
	public static void studentpassword() {
		String Ipassword;
		String CorrectPassword = "931026'Wade";
		boolean bresult2 = false;
		Ipassword = JOptionPane.showInputDialog("Enter Your Password");
		if (Ipassword.equals(CorrectPassword)) {
			bresult2 = true;
			JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                          "Welcome", JOptionPane.PLAIN_MESSAGE);
		} else if ((count >= 2) && (bresult2 == false)) {
			JOptionPane.showMessageDialog(null,
                                          "Account Locked. Contact Admin", "Locked",
                                          JOptionPane.ERROR_MESSAGE);
		} else
			while (count < 2) {
				count++;
				Ipassword = JOptionPane
                .showInputDialog("Wrong password.Reenter Your Password");
				if (Ipassword.equals(CorrectPassword)) {
					JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                                  "Welcome", JOptionPane.PLAIN_MESSAGE);
					break;
				} else if ((count == 2) && (bresult2 == true)) {
					JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                                  "Welcome", JOptionPane.PLAIN_MESSAGE);
					break;/*
                           * solve the problem when count = 2 & bresult2 = 2, the
                           * application shows up with both welcome and account
                           * locked
                           */
				} else {
					bresult2 = false;
				}
				if ((count >= 2) && (bresult2 == false)) {
					JOptionPane.showMessageDialog(null,
                                                  "Account Locked. Contact Admin", "Locked",
                                                  JOptionPane.ERROR_MESSAGE);
				}
			}
	}
    
	public static void Staff() {
		boolean bresult1 = false;
		Iusername = JOptionPane.showInputDialog("Enter Your Username");
		if (Iusername.equals(StudentUsername)) {
			AccountType();
		} else if (Iusername.equals(AdminUsername)) {
			AccountType();
		} else if (Iusername.equals(StaffUsername)) {
			bresult1 = true;
		} else {
			bresult1 = false;
		}
		if (bresult1 == true) {
			staffpassword();
		} else if (bresult1 == false) {
			while (count < 2) {
				count++;
				Iusername = JOptionPane
                .showInputDialog("Username doesn't exist.Reenter Your Username");
				if (Iusername.equals(StaffUsername)) {
					staffpassword();
					bresult1 = true;
					break;
				} else if ((count == 2) && (bresult1 == true)) {
					staffpassword();
					break;
				} else {
					bresult1 = false;
				}
			}
			if ((count >= 2) && (bresult1 == false)) {
				JOptionPane.showMessageDialog(null,
                                              "Invalid username. Contact Admin", "Locked",
                                              JOptionPane.ERROR_MESSAGE);
			}
		}
	}
    
	public static void staffpassword() {
		String Ipassword;
		String CorrectPassword = "Thanes'050999";
		boolean bresult2 = false;
		Ipassword = JOptionPane.showInputDialog("Enter Your Password");
		if (Ipassword.equals(CorrectPassword)) {
			bresult2 = true;
			JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                          "Welcome", JOptionPane.PLAIN_MESSAGE);
		} else if ((count >= 2) && (bresult2 == false)) {
			JOptionPane.showMessageDialog(null,
                                          "Account Locked. Contact Admin", "Locked",
                                          JOptionPane.ERROR_MESSAGE);
		} else
			while (count < 2) {
				count++;
				Ipassword = JOptionPane
                .showInputDialog("Wrong password.Reenter Your Password");
				if (Ipassword.equals(CorrectPassword)) {
					JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                                  "Welcome", JOptionPane.PLAIN_MESSAGE);
					break;
				} else if ((count == 2) && (bresult2 == true)) {
					JOptionPane.showMessageDialog(null, "Welcome " + Iusername,
                                                  "Welcome", JOptionPane.PLAIN_MESSAGE);
					break;/*
                           * solve the problem when count = 2 & bresult2 = 2, the
                           * application shows up with both welcome and account
                           * locked
                           */
				} else {
					bresult2 = false;
				}
				if ((count >= 2) && (bresult2 == false)) {
					JOptionPane.showMessageDialog(null,
                                                  "Account Locked. Contact Admin", "Locked",
                                                  JOptionPane.ERROR_MESSAGE);
				}
			}
	}
    
}