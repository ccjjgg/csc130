/*
Yusi Cheng@csc130
2014
Assignment 4
 */
package assignment4;
/*
 *@author Justin0509
 */
public class Assignment4 {

    /**
     * @param args the command line arguments
     */
	public static void main(String[] args) {
		// Room 1
		room FirstRoom = new room();
		FirstRoom.SetWalls("Yellow");
		FirstRoom.SetFloors("Hardwood");
		FirstRoom.setNumwindows(1);
		FirstRoom.output();

		// Room 2
		room SecondRoom = new room("Purple", "Tiled", 0);
		SecondRoom.output();

		// Room 3
		room ThirdRoom = new room("White ", "Carpet", 3);
		ThirdRoom.output();

	}
}