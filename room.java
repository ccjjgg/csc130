/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package assignment4;

/**
 *
 * @author Justin0509
 */
public class room 
{
//Datafields
	public String Walls; //Color of the Wall
	public String Floors; //Type of FLoor
	public int numWindows; //Number of Windows

	//Defaultconstructor
	public room()
	{
	Walls= "white";
	Floors = "wood";
	numWindows= 0;
	}
	
	//Override constructor
	public room(String wall, String floor, int num)
	{
	Walls= wall;
	Floors= floor;
	numWindows= num;
	}
	
	//Set Walls
	public void SetWalls (String wall)
	{Walls = wall;
	}
	
	//Set Floors
	public void SetFloors (String floor)
	{Floors= floor;
	
	}
	
	//Set numWindows
	public void setNumwindows (int num)
	{
	numWindows= num;
		
	}
	
	//Get Walls
	public String getWalls()
	{
	return Walls;
	}
	
	//Get Floor
	public String getFloors()
	
	{
	return Floors;
	
	}
	
	//Get numWindows
	public int getNumWindows()
	{
	return numWindows;
	}
	
	public void output()
	{
	System.out.println ("Wall color: " + Walls +", Floor Type: " + Floors 
                +", Number of Windows: "+ numWindows);
	
	}
}